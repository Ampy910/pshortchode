<?php
/*
Plugin Name: Data
Plugin URI: http://wordpress.org/plugins/hello-dolly/
Description: pasar datos
Author: Matt Mullenweg
Version: 0.0.1
Author URI: http://ma.tt/
*/



function ShortCodeInfo($atts){
    ob_start();
    $type = $atts["task"];
    $args = array(
        'numberposts' => -1,
        'post_type' => 'tareas',  
    );

    $posts_ids = [];
    $the_query = new WP_Query( $args );
    if($the_query->have_posts() ) : 
        while ( $the_query->have_posts() ) : 
            $the_query->the_post(); 
            $typeTask = get_post_meta(get_the_ID(),'estado',true);
            if($type == $typeTask){
                $posts_ids[] = get_the_ID();
            }
        endwhile; 
        wp_reset_postdata(); 
    endif; 
    foreach ($posts_ids as $key => $post_id) {
        $task = get_post($post_id);
        $title = get_the_title($post_id);
        $description = get_the_content(null, false,$post_id );


        ?>
        <div class="titulo">
            <h6 class='margin' ><?=$title?></h6>
            
            <a class='createform' href="">+</a>
        </div>

        <?=$description?>
        <?php
    }
    ?>
    
    
    <?php
    return ob_get_clean();
}


add_shortcode('shortInfo' , 'ShortCodeInfo')

//Tickets
?>

